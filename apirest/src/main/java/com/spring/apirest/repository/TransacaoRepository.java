package com.spring.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.apirest.models.Transacao;

public interface TransacaoRepository extends JpaRepository<Transacao, Long> {
	
}
