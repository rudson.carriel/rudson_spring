package com.spring.apirest.resources;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.apirest.models.Transacao;
import com.spring.apirest.repository.TransacaoRepository;



@RestController
public class TransacaoResource {
	
	@Autowired
	TransacaoRepository transacaoRepository;
	
	//metodo get all
	@GetMapping("/transaction/all")
	public List<Transacao> listaTransacoes(){
		return transacaoRepository.findAll();
		
	}
	
	//metodo get por id
	@GetMapping("/transaction/{id}")
	public Optional<Transacao> listaTransacao(@PathVariable(value="id")long id ){
		return transacaoRepository.findById(id);
	}
	
	
	//metodo post
	@PostMapping("/transaction")
	public Transacao saveTransacao(@RequestBody Transacao transacao) {
		return transacaoRepository.save(transacao);
	}
	
	//metodo put
	@PutMapping("/transaction")
	public Transacao updateTransacao(@RequestBody Transacao transacao) {
		return transacaoRepository.save(transacao);
		
	}
	
}
